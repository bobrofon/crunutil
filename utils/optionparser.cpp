#include "optionparser.h"
#include <stdexcept>
#include <iostream>
#include <cstdlib>
#include <unistd.h>

using namespace std;

namespace Utils {

OptionParser::OptionParser(int argc, char *argv[]) {
	app_name = argv[0];
	Parse(argc, argv);
	try {
		ValidateConstrains();
	} catch (const invalid_argument &e) {
		ShowErrorHelpAndExit(e.what());
	}
}

void OptionParser::Parse(int argc, char *argv[]) {
	int opt;
	while ((opt = getopt(argc, argv, "hf:m:v:")) != -1) {
		switch (opt) {
		case 'h':
			ShowHelpAndExit();
			break;
		case 'f':
			file_name = optarg;
			break;
		case 'm':
			module = optarg;
			break;
		case 'v':
			pattern = optarg;
			break;
		default:
			exit(EXIT_FAILURE);
		}
	}
}

void OptionParser::ValidateConstrains() const {
	if (file_name.empty()) {
		throw invalid_argument("Missing required option '-f'");
	}
	if (module.empty()) {
		throw invalid_argument("Missing required option '-m'");
	}
	if (module != Modules::WORDS && module != Modules::CHECKSUM) {
		throw invalid_argument("Invalid option value " + module);
	}
	if (module == Modules::WORDS && pattern.empty()) {
		throw invalid_argument("Missing required option '-v'");
	}
}

void OptionParser::ShowErrorHelpAndExit(const string& msg) const {
	cerr << app_name << ": " << msg << endl
		 << "Try '" << app_name << " -h' for more information." << endl;
	exit(EXIT_FAILURE);
}

void OptionParser::ShowHelpAndExit() const {
	cout << "Usage: " << app_name << " -f filename -m (" << Modules::CHECKSUM << " | (" << Modules::WORDS << " -v pattern))" << endl
		<< "Command-line utility to inspect files." << endl
		<< endl
		<< "Options:" << endl
		<< "  -f=VALUE                   input file" << endl
		<< "  -m=VALUE                   " << Modules::WORDS << " or " << Modules::CHECKSUM << endl
		<< "  -v=VALUE                   search pattern (only for " << Modules::WORDS << " method)" << endl
		<< "  -h                         show this message and exit" << endl;
	exit(EXIT_SUCCESS);
}

} /*Utils*/
