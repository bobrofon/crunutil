#ifndef OPTIONPARSER_H
#define OPTIONPARSER_H

#include "constants.h"

namespace Utils {

class OptionParser {
private:
	std::string module;
	std::string file_name;
	std::string pattern;
	std::string app_name;

	void ValidateConstrains() const;
	void ShowErrorHelpAndExit(const std::string& msg) const;
	void ShowHelpAndExit() const;
	void Parse(int argc, char *argv[]);

public:
	OptionParser(int argc, char *argv[]);

	inline const std::string& Module() const {
		return module;
	}

	inline const std::string& FileName() const {
		return file_name;
	}

	inline const std::string& Pattern() const {
		return pattern;
	}
};

} /*Utils*/

#endif // OPTIONPARSER_H
