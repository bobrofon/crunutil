#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

namespace Utils {

struct Modules {
	static const std::string WORDS;
	static const std::string CHECKSUM;
};

struct Default {
	static constexpr size_t BUFFER_SIZE = 2 * 1024 * 1024;
};

} /*Utils*/

#endif // CONSTANTS_H
