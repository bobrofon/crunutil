# README #

CRunUtil is a command-line utility to inspect files.

### Usage ###
```
$ ./CRunUtil -h
Usage: ./CRunUtil -f filename -m (checksum | (words -v pattern))
Command-line utility to inspect files.

Options:
  -f=VALUE                   input file
  -m=VALUE                   words or checksum
  -v=VALUE                   search pattern (only for words method)
  -h                         show this message and exit
```
### Режимы работы ###
```
CRunUtil -f Test.tst -m words -v mother
```
печатает количество слов «mother» в файле «Test.tst»

```
CRunUtil -f Test.tst -m checksum
```
печатает 32-хбитную чексумму, рассчитанную по алгоритму checksum = word1 + word2 + … + wordN (word1..wordN – 32-хбитные слова, представляющие содержимое файла)

```
CRunUtil –h
```
печатает информацию о программе и описание параметров.