#include <iostream>
#include <fstream>
#include <cstdlib>
#include "utils/optionparser.h"
#include "modules/words.h"
#include "modules/checksum.h"

using namespace std;

int main(int argc, char *argv[]) {
	Utils::OptionParser options(argc, argv);

	ifstream stream(options.FileName(), (options.Module() == Utils::Modules::CHECKSUM ? (ios::in|ios::binary) : ios::in));
	if (!stream) {
		cerr << "Can't open file " << options.FileName() << endl;
		return EXIT_FAILURE;
	}

	if (options.Module() == Utils::Modules::WORDS) {
		cout << Modules::Words(stream, options.Pattern()) << endl;
	} else if (options.Module() == Utils::Modules::CHECKSUM) {
		cout << Modules::Checksum(stream) << endl;
	}

	return EXIT_SUCCESS;
}

