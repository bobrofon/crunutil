#include "words.h"

#include <iterator>
#include <algorithm>

using namespace std;

namespace Modules {

int Words(istream& stream, const string& pattern) {
	istream_iterator<string> eos;
	istream_iterator<string> iit(stream);
	return count(iit, eos, pattern);
}

} /*Modules*/
