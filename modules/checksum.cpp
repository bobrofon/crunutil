#include "checksum.h"
#include <array>
#include <numeric>
#include <algorithm>
#include "utils/constants.h"

using namespace std;

namespace Modules {

uint32_t Checksum(istream& stream) {
	array<char,Utils::Default::BUFFER_SIZE> buffer;
	uint32_t *intBuffer = reinterpret_cast<uint32_t*>(buffer.data());
	size_t intBufferSize = sizeof(buffer) / sizeof(*intBuffer);
	uint32_t checksum = 0;

	while (stream.read(buffer.data(), buffer.size())) {
		checksum = accumulate(intBuffer, intBuffer + intBufferSize, checksum);
	}
	fill(begin(buffer) + stream.gcount(), end(buffer), 0);
	return accumulate(intBuffer, intBuffer + intBufferSize, checksum);
}

}
