#ifndef CHECKSUM_H
#define CHECKSUM_H

#include <cstdint>
#include <istream>

namespace Modules {

/*
 * Checksum computes 32-bit hash for stream.
 * stream represented as a sequence of integers in big-endian format.
 */
uint32_t Checksum(std::istream& stream);

}

#endif // CHECKSUM_H
