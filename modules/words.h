#ifndef WORDS_H
#define WORDS_H

#include <istream>
#include <string>

namespace Modules {

/*
 * Words returns count of matching pattern into the stream.
 */
int Words(std::istream& stream, const std::string& pattern);

} /*Modules*/

#endif // WORDS_H
