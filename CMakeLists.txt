project(CRunUtil)
cmake_minimum_required(VERSION 2.8)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
include_directories(.)
aux_source_directory(. SRC_LIST)
aux_source_directory(./utils SRC_LIST)
aux_source_directory(./modules SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})

